'''
This code is released into the public domain per the Unlicense
https://unlicense.org

@package skyeHash
'''

import sys
import hashlib

prefix="OnCouIGyoqqTCvvf_www.oncourseconnect.com_20210609-2305_11349573_"
knownHash="4aa64c0a248c3855bdd33c2c490ff9cbe901071de2ea247cf5985c668b662230"
guess="0000000000000000000000000000000000000000"

try:
    while str(hashlib.sha256(bytes(prefix + guess, 'utf-8')).hexdigest()).lower() != knownHash:
        guess = "{0:040x}".format(int(guess, 16) + 1)

except KeyboardInterrupt:
    print("Stopped execution!")

print(guess + " is the hash you were looking for!")
